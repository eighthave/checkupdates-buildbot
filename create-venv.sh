#!/bin/sh -ex

python3 -m venv --clear --system-site-packages venv
. venv/bin/activate
pip3 install wheel
pip3 install --upgrade \
    buildbot-console-view==3.5.0 \
    buildbot-grid-view==3.5.0 \
    buildbot-waterfall-view==3.5.0 \
    buildbot-www==3.5.0 \
    buildbot-worker==3.5.0 \
    buildbot==3.5.0 \

#    service_identity

printf "\nRun:\nsource `pwd`/venv/bin/activate\n"
