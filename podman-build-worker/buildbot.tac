# -*- python -*-
# ex: set filetype=python:

import os
import yaml

from pathlib import Path
from buildbot_worker.bot import Worker
from twisted.application import service

basedir = os.path.abspath(os.path.dirname(__file__))

with (Path(basedir).parent / 'passwords.yaml').open() as fp:
    passwords = yaml.safe_load(fp)

rotateLength = 10000000
maxRotatedFiles = 10

# note: this line is matched against to check that this is a worker
# directory; do not edit it.
application = service.Application('buildbot-worker')

from twisted.python.logfile import LogFile
from twisted.python.log import ILogObserver, FileLogObserver

logfile = LogFile.fromFullPath(
    os.path.join(basedir, "twistd.log"),
    rotateLength=rotateLength,
    maxRotatedFiles=maxRotatedFiles,
)
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)

buildmaster_host = '127.0.0.1'
port = 9989
workername = 'podman-build'
passwd = passwords['workers']['podman-build']
keepalive = 600
umask = None
maxdelay = 300
numcpus = None
allow_shutdown = None
maxretries = None
use_tls = 0
delete_leftover_dirs = 0
proxy_connection_string = None

s = Worker(
    buildmaster_host,
    port,
    workername,
    passwd,
    basedir,
    keepalive,
    umask=umask,
    maxdelay=maxdelay,
    numcpus=numcpus,
    allow_shutdown=allow_shutdown,
    maxRetries=maxretries,
    useTls=use_tls,
    delete_leftover_dirs=delete_leftover_dirs,
    proxy_connection_string=proxy_connection_string,
)
s.setServiceParent(application)
