
This is a Buildbot setup for running `fdroid checkupdates` per-app in a
disposable container.  It is currently built on Buildbot and Docker, as packaged
in Debian/bullseye.


## "master" naming

We would prefer using a better name than "master" in our Buildbot setup.
Unfortunately, the "master" naming is so tied up in the Buildbot source code, it
would [take months of
work](https://github.com/buildbot/buildbot/issues/5382#issuecomment-647962876)
to fix.  So until that is fixed, we are stuck with the name "master".
