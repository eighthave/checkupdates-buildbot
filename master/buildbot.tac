# -*- python -*-
# ex: set filetype=python:

import os
import yaml

from pathlib import Path
from buildbot.master import BuildMaster
from twisted.application import service

basedir = os.path.abspath(os.path.dirname(__file__))

# TODO if file doesn't exist, create it and gen passwords
passwords_yaml = Path(basedir).parent / 'passwords.yaml'
if not passwords_yaml.exists():
    d = {
        'workers': {'podman-build': 'Ban9xo4jieTh7Ku3xookocheeyaegei3'},
        'change_source': {'checkupdates': 'ohl3acuD0looCiengob3abae3eiquaec'},
    }
    with passwords_yaml.open('w') as fp:
        yaml.dump(d, fp)

with passwords_yaml.open() as fp:
    passwords = yaml.safe_load(fp)

rotateLength = 10000000
maxRotatedFiles = 10
configfile = 'master.cfg'

# Default umask for server
umask = None

# note: this line is matched against to check that this is a buildmaster
# directory; do not edit it.
application = service.Application('buildmaster')
from twisted.python.logfile import LogFile
from twisted.python.log import ILogObserver, FileLogObserver

logfile = LogFile.fromFullPath(
    os.path.join(basedir, "twistd.log"),
    rotateLength=rotateLength,
    maxRotatedFiles=maxRotatedFiles,
)
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)

m = BuildMaster(basedir, configfile, umask)
m.setServiceParent(application)
m.log_rotation.rotateLength = rotateLength
m.log_rotation.maxRotatedFiles = maxRotatedFiles
